Magento PHPUnit Integration
===========================



#System Requirements
-------------------

* PHP 5.3 or higher
* PHPUnit 3.7.x
* Magento CE1.4.x-1.7.x/PE1.9.x-PE1.10.x/EE1.9.x-1.12.x


#Links
-------------------

* Original module: [github link](https://github.com/EcomDev/EcomDev_PHPUnit)
* Module documentation: [Documentation-0.2.0](http://www.ecomdev.org/wp-content/uploads/2011/05/EcomDev_PHPUnit-0.2.0-Manual.pdf)
* Example of test module with phpunit tests: [Test Module](https://www.dropbox.com/s/2qyinu92ix05an9/TestModule.zip)
* Magento Composer Installer Project: [github link](https://github.com/magento-hackathon/magento-composer-installer)
* Installing PHPUnit via PEAR or Composer: [Installing PHPUnit](http://phpunit.de/manual/current/en/installation.html)
* Magento Modman Project: [github link](https://github.com/colinmollenhour/modman)
* PHPUnit documentation ru: [documentation](http://phpunit-doc.verber.kh.ua/3.7/ru/index.html)

#Composer Installation
-------------------

* $ curl -sS https://getcomposer.org/installer | php
* $ mv composer.phar /usr/local/bin/composer

Windows: [windows installation](http://getcomposer.org/doc/00-intro.md#installation-windows)

Magento and Composer (additional links):
-------------------
* [How to make Magento Extensions work with Composer](http://magebase.com/magento-tutorials/how-to-make-magento-extensions-work-with-composer/)
* [Composer with Magento](http://magebase.com/magento-tutorials/composer-with-magento/)


#Module installation:
-------------------

Example of magento instance directory (before module installation):

```bash
- magento-project
  - htdocs (magento files)
  - composer.json
```
composer.json
------
```bash
 { 
  "minimum-stability":"dev",
  "require":{
    "cm1/magento-phpunit": "*",
    "phpunit/phpunit":"3.7.*",
    "phpunit/dbunit": ">=1.2",
    "phpunit/php-invoker": "*",
    "phpunit/phpunit-selenium": ">=1.2",
    "phpunit/phpunit-story": "*"
  },
  "repositories":[
    {
      "type":"vcs",
      "url":"https://vkubrak@bitbucket.org/gorilla-cm1/magento-phpunit.git"
    },
    {
       "type":"vcs",
       "url":"https://github.com/magento-hackathon/magento-composer-installer"
    }
  ],
  "extra":{
    "magento-root-dir":"./htdocs"
  }
}
```
In case you prefer copies to symlinks, no problem: simply set the appropriate config option under extra.magento-deploystrategy:

```bash
{
  ...
  "extra":{
    "magento-root-dir":"./htdocs",
    "magento-deploystrategy":"copy"
  }
}
```

Run composer installation:

```bash
cd /var/www/magento-project
composer install
```
Example of magento instance directory (after module installation):

```bash
- magento-project
  - htdocs (magento files)
  - vendor (modules files)
  - composer.json
  - composer.lock
```

After installing module, run initial installation scripts:

```bash
cd /var/www/magento-project/htdocs/shell
# Run shell installer
php -f ecomdev-phpunit.php -- --action install
# Disables built in tests for extension
php -f ecomdev-phpunit.php -- --action change-status
# Specify your test database name and base url for controller tests
php ecomdev-phpunit.php -a magento-config --db-name $DB_NAME --base-url http://your.magento.url/
```

Run phpunut
```bash
cd /var/www/magento-tests/htdocs
../vendor/bin/phpunit
```

If it shows that there was no tests found, it means that extension was successfully installed. If it shows some errors,
then it means, that your customizations has install scripts that relay on your current database data and you should fix them. Or use your dev database as a base for the tests, but prior first time running the suite.


Test module with phpunit tests:
---------

[Test Module Donwnload](https://www.dropbox.com/s/2qyinu92ix05an9/TestModule.zip)