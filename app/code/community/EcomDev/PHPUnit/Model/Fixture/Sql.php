<?php
/**
 * Class EcomDev_PHPUnit_Model_Fixture_Sql
 */
class EcomDev_PHPUnit_Model_Fixture_Sql extends EcomDev_PHPUnit_Model_Fixture
{
    const DATA_DIR = '_data';

    /**
     * @var array
     */
    protected $sqlProcessors = array(
        'sql' => 'ecomdev_phpunit/fixture_processor_sql'
    );

    /**
     * @var array
     */
    protected $_typeMap = array(
        'fixtures'     => 'fx',
        'providers'    => 'dp',
        'expectations' => 'ex'
    );

    /**
     * Returns list of available processors for fixture
     *
     * @return EcomDev_PHPUnit_Model_Fixture_ProcessorInterface[]
     */
    public function getProcessors()
    {
        if (empty($this->_processors)) {
            foreach ($this->sqlProcessors as $code => $processorAlias) {
                $processor = Mage::getSingleton((string)$processorAlias);
                if ($processor instanceof EcomDev_PHPUnit_Model_Fixture_ProcessorInterface) {
                    $this->_processors[$code] = $processor;
                }
            }
        }

        return $this->_processors;
    }

    /**
     * Applies loaded fixture
     *
     * @return EcomDev_PHPUnit_Model_Fixture
     */
    public function apply()
    {
        $processors = $this->getProcessors();
        // Initialize fixture processors
        foreach ($processors as $processor) {
            $processor->initialize($this);
        }

        $this->setStorageData(self::STORAGE_KEY_FIXTURE, $this->_fixture);
        $processors['sql']->apply(array($this->_fixture), null, $this);

        // Clear fixture for getting rid of double processing
        $this->_fixture = array();
        return $this;
    }

    /**
     * Reverts environment to previous state
     *
     * @return EcomDev_PHPUnit_Model_Fixture
     */
    public function discard()
    {
        $fixture = $this->getStorageData(self::STORAGE_KEY_FIXTURE);

        if (!is_array($fixture)) {
            $fixture = array();
        }

        $this->_fixture = $fixture;
        $this->setStorageData(self::STORAGE_KEY_FIXTURE, null);

        $processors = $this->getProcessors();
        $processors['sql']->discard($this->_fixture, null, $this);

        $this->_fixture = array();
    }


    /**
     * Loads fixture files
     *
     * @param array                            $fixtures
     * @param string|EcomDev_PHPUnit_Test_Case $classOrInstance
     *
     * @throws RuntimeException
     * @return EcomDev_PHPUnit_Model_Fixture
     */
    protected function _loadFixtureFiles(array $fixtures, $classOrInstance)
    {
        $isShared = ($this->isScopeShared() || !$classOrInstance instanceof PHPUnit_Framework_TestCase);
        foreach ($fixtures as $fixture) {
            if (empty($fixture) && $isShared) {
                $fixture = self::DEFAULT_SHARED_FIXTURE_NAME;
            } elseif (empty($fixture)) {
                $fixture = $classOrInstance->getName(false);
            }

            $className = (is_string($classOrInstance) ? $classOrInstance : get_class($classOrInstance));
            $filePath  = $this->_resolveFilePath(
                $fixture, $className, EcomDev_PHPUnit_Model_Yaml_Loader::TYPE_FIXTURE
            );

            if (!$filePath) {
                throw new RuntimeException('Unable to load fixture for test: '.$fixture);
            }

            $this->loadSqlQuery($filePath);
        }

        return $this;
    }

    /**
     * Load YAML file
     *
     * @param string $filePath
     * @return EcomDev_PHPUnit_Model_Fixture
     * @throws InvalidArgumentException if file is not a valid YAML file
     */
    public function loadSqlQuery($filePath)
    {
        $data = file_get_contents($filePath);

        if (empty($this->_fixture)) {
            $this->_fixture = $data;
        }

        return $this;
    }

    /**
     * @param $fileName
     * @param $relatedClassName
     * @param $type
     * @return bool
     */
    protected function _resolveFilePath($fileName, $relatedClassName, $type)
    {
        if (strrpos($fileName, '.sql') !== strlen($fileName) - 5) {
            $fileName .= '.sql';
        }

        $filePath = $this->_getFilePath($fileName, $relatedClassName, $type);

        if ($filePath && file_exists($filePath)) {
            return $filePath;
        }

        return false;
    }

    /**
     * Returns processed file path
     *
     * @param string $fileName
     * @param string $relatedClassName
     * @param string $type
     * @return string|bool
     */
    protected function _getFilePath($fileName, $relatedClassName, $type)
    {
        $reflection = EcomDev_Utils_Reflection::getReflection($relatedClassName);
        $fileObject = new SplFileInfo($reflection->getFileName());

        return $this->_checkFilePath(array(
            $fileObject->getPath(),
            $fileObject->getPath() . DS . $fileObject->getBasename('.php')
        ), $fileName, $type);
    }

    /**
     * Looks in path for possible existent fixture
     *
     * @param string|array $path
     * @param string $fileName
     * @param string $type
     * @return bool|string
     */
    protected function _checkFilePath($path, $fileName, $type)
    {
        if (is_array($path)) {
            foreach ($path as $value) {
                if ($filePath = $this->_checkFilePath($value, $fileName, $type)) {
                    return $filePath;
                }
            }

            return false;
        }

        if (isset($this->_typeMap[$type])
            && file_exists($filePath = $path . DS . self::DATA_DIR . DS . $this->_typeMap[$type] . '-' . $fileName)) {
            return $filePath;
        }

        if (file_exists($filePath = $path . DS . $type . DS . $fileName)) {
            return $filePath;
        }

        return false;
    }
}