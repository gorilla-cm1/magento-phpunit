<?php
/**
 * Class EcomDev_PHPUnit_Model_Fixture_Processor_Sql
 */
class EcomDev_PHPUnit_Model_Fixture_Processor_Sql
    extends Mage_Core_Model_Abstract
    implements EcomDev_PHPUnit_Model_Fixture_ProcessorInterface
{
    /**
     * @var array
     */
    protected $tablesForTruncate = array();

    /**
     * Initializes fixture processor before applying data
     *
     * @param EcomDev_PHPUnit_Model_FixtureInterface $fixture
     * @return $this
     */
    public function initialize(EcomDev_PHPUnit_Model_FixtureInterface $fixture)
    {
        return $this;
    }

    /**
     * Applies data from fixture file
     *
     * @param array[] $data
     * @param string $key
     * @param EcomDev_PHPUnit_Model_FixtureInterface $fixture
     *
     * @return EcomDev_PHPUnit_Model_Fixture_ProcessorInterface
     */
    public function apply(array $data, $key, EcomDev_PHPUnit_Model_FixtureInterface $fixture)
    {
        $sql = current($data);
        if (empty($sql)) {
            return $this;
        }

        if (preg_match_all('/((INTO|UPDATE) `(.*)`)/', $sql, $matches)) {
            $tables = array_unique($matches[3]);
            array_walk($tables, function (&$value, $key) {
               $value = current(explode(' ', str_replace('`', '',trim($value))));
            });

            $this->tablesForTruncate = $tables;
        }

        $setup = new Mage_Catalog_Model_Resource_Setup('core_write');
        $setup->startSetup()
            ->run($sql)
            ->endSetup();
    }

    /**
     * Discard applied eav records
     *
     * @param array[] $data
     * @param string $key
     * @param EcomDev_PHPUnit_Model_FixtureInterface $fixture
     *
     * @return EcomDev_PHPUnit_Model_Fixture_Processor_Eav
     */
    public function discard(array $data, $key, EcomDev_PHPUnit_Model_FixtureInterface $fixture)
    {
        if (!count($this->tablesForTruncate)) {
            return $this;
        }

        $setup = new Mage_Catalog_Model_Resource_Setup('core_write');
        foreach ($this->tablesForTruncate as $table) {
            $setup->startSetup()
                ->run("TRUNCATE `{$setup->getTable($table)}`")
                ->endSetup();
        }
        return $this;
    }
}